import request from './requests';

const loading = (type, isLoading) => ({
    type,
    isLoading
})

const error = (type, error) => ({
    type,
    error
})

const success = (type, response) => ({
    type, 
    response
})

const factory = (actionLoading, actionError, actionSuccess, requestInfo) => async (dispatch) => {
    try {
        //loading
        dispatch(loading(actionLoading, true));
        //faz pedido

        const response = await request(requestInfo.url, requestInfo.method, requestInfo.data);

        dispatch(loading(actionLoading, false));
        if (response.error) {
            return dispatch(error(actionError, response.response));
        }

        //successo
        dispatch(success(actionSuccess, response.response));
    } catch (e) {
        dispatch(loading(actionLoading, false));
        dispatch(error(actionError, e));
    }
}

export default factory;