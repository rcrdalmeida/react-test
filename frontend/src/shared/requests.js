const AUTHORIZATION = 'ricardo';

const request = async (url, method, data) => {
    try {
        const request = await fetch(url, {
            method,
            headers: {
                'Authorization': AUTHORIZATION,
                'Content-Type': 'application/json'
            },
            body: data !== undefined ? JSON.stringify(data) : data
        });

        const response = await request.json();
        console.log(response)
        return {
            status: request.status,
            error: request.status < 200 || request.status > 300,
            response
        }
    } catch (error) {
        console.error(error);
        throw error;
    }
}

export default request;