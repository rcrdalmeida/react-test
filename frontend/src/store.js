import { applyMiddleware, createStore, combineReducers, compose } from 'redux';
import thunk from 'redux-thunk';

import categoriesReducer from './containers/Categories/reducer';
import postsReducer from './containers/Posts/reducer';

const reducers = combineReducers({
    categoriesReducer: categoriesReducer,
    postsReducer: postsReducer
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(reducers, composeEnhancers(applyMiddleware(thunk)));

export default store;