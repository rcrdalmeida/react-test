import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import * as serviceWorker from './serviceWorker';
import FrontPage from './containers/FrontPage/FrontPage';
import store from './store';
import {context} from './containers/useModal';

ReactDOM.render(
  <Provider store={store}>
    <context.Provider
      value={{
        subscribers: {}
      }}
    >
      <FrontPage />
    </context.Provider>
  </Provider>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
