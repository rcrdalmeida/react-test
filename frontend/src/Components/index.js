import Button from './UI/Button';
import Modal from './UI/Modal'
import Input from './UI/Input';
import Select from './UI/Select';
import TextArea from './UI/TextArea';

export {
    Button,
    Modal,
    Input,
    Select,
    TextArea
}