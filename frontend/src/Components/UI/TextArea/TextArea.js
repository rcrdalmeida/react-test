import React from 'react';
import styles from "./styles.module.css";

const TextArea = (props) => (
    <textarea 
        className={styles.textArea}
        name={props.fieldName}
        onChange={(event) => props.onChange(event.target.value)}
        value={props.value}
        placeholder={props.placeholder}
        required={props.required}
    />
)

export default TextArea;