import React from 'react';
import styles from './styles.module.css';

const Select = (props) => (
    <select className={styles.selectField} name={props.fieldName} value={props.selected} onChange={(event) => props.onChange(event.target.value)} required={props.required}>
        <option value=''>Choose a category</option>
        {props.options.length > 0 && props.options.map((opt) => <option key={opt.value} value={opt.value}>{opt.name}</option>)}
    </select>
)

export default Select;