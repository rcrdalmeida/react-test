import React from 'react';
import PropTypes from 'prop-types';

const Button = ({children, onClick, styleButton, styleSpan}) => {
    return (
        <button onClick={onClick} className={styleButton}>
            <i className={styleSpan} style={{marginRight: "5px"}} aria-hidden="true"></i>
                {children}
        </button>
    );
}

Button.propTypes = {
    children: PropTypes.any,
    onClick: PropTypes.func,
    styleButton: PropTypes.string,
    styleSpan: PropTypes.string
}

Button.defaultProps = {
    onClick: () => {},
    styleButton: "",
    styleSpan: ""
};

export default Button;