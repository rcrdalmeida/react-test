import React from 'react';
import PropTypes from 'prop-types';
import styles from './styles.module.css'

const Input = (props) => {
     return (
        <div className={styles.box}>
            <input name={props.fieldName} value={props.value} placeholder={props.placeholder} style={props.style} onChange={event => props.onChange(event.target.value)} required={props.required}>
            </input>
        </div>
    );
}

Input.propTypes = {
    fieldName: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired,
    value: PropTypes.string,
    required: PropTypes.bool
}

Input.defaultProps = {
    required: false
}

export default Input;