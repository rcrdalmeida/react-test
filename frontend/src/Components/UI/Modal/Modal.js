import React from 'react';
import styles from "./styles.module.css";

const Modal = ({children, closable, onClose, styleContent}) => {
    return (
        <div className={styles.modal}>
            <div className={styles.content} style={styleContent}>
                {closable && <span onClick={onClose} className={styles.close}>&times;</span>}
                {children}
            </div>
        </div>
    );
}

export default Modal;