import React from 'react';
import {useSelector} from 'react-redux';
import PropTypes from 'prop-types';

import { useModal } from '../useModal';
import { Modal, Input, Button, Select, TextArea } from '../../Components';
import { Form, withForm } from '../Form';
import styles from './styles.module.css';

const Fields = {
    Input: withForm(Input),
    Select: withForm(Select),
    TextArea: withForm(TextArea)
}

const PostModal = ({editPost, onSubmitPostHandler}) => {
    const [postModal, setPostModal] = useModal('post');

    const categories = useSelector(state => state.categoriesReducer.categories);
    const selectedCategory = useSelector(state => state.categoriesReducer.category);
    const onSubmitHandler = (values) => {
        setPostModal(false);

        const post = {
            title: values.title,
            body: values.body,
            author: sessionStorage.getItem('user'),
            category: values.category
        }
        onSubmitPostHandler(post);
    }

    return (
        <div>
        {postModal && <Modal closable onClose={() => setPostModal(false)}>
                {!!editPost.category ? <h2>Edit post</h2> : <h2>Submit post</h2>}
                <Form onSubmit={onSubmitHandler} info={editPost}>
                    <Fields.Select 
                        options={categories.filter(cat => cat.name !== 'All').map(cat => ({value: cat.path, name: cat.name}))} 
                        selected={editPost.category || selectedCategory}
                        fieldName='category'
                        required
                    />
                    <Fields.Input 
                        value={editPost.title || ''}
                        fieldName='title'
                        placeholder='Insert title'
                        required
                        
                    />
                    <Fields.TextArea 
                        fieldName='body'
                        placeholder='Insert some text'
                        value={editPost.body || ''}
                        required
                    />
                    <div className={styles.buttonsContainer}>
                        <Button styleButton={styles.button} onClick={() => setPostModal(false)}>Cancel</Button>
                        <Button styleButton={[styles.button,styles.submit].join(' ')}>Submit</Button>
                    </div>
                    
                </Form>
            </Modal> 
        }
    </div>
    )
};

PostModal.propTypes = {
    editPost: PropTypes.object,
    onSubmitPostHandler: PropTypes.func.isRequired
}

PostModal.defaultProps = {
    editPost: {}
}

export default PostModal;