import React, {useEffect} from 'react';
import {useDispatch, useSelector} from 'react-redux';

import Categories from '../Categories/Categories';
import Posts from '../Posts/Posts';
import * as postsActionCreators from '../Posts/actionCreators';
import { Button } from '../../Components';
import UserModal from '../UserModal/UserModal';
import PostModal from '../PostModal';
import { useModal } from '../useModal';
import styles from './styles.module.css'

const FrontPage = () => {
    const dispatch = useDispatch();
    const [, setPostModal] = useModal('post');

    const selectedPost = useSelector(state => state.postsReducer.selectedPost);

    useEffect(() => {
        dispatch(postsActionCreators.getPosts());
    }, [dispatch]);

    const onSubmitPostHandler = (values) => {
        if (selectedPost !== '') {
            return dispatch(postsActionCreators.updatePost(selectedPost, values))
        }
       return dispatch(postsActionCreators.submitPost(values));
    }

    const onClickHandler = () => {
        dispatch(postsActionCreators.setSelectedPost(''));
        setPostModal(true)
    }

    const posts = useSelector((state) => state.postsReducer.posts);
    const [postToEdit] = posts.filter(post => post.id === selectedPost);

    return (
        <React.Fragment>
            <UserModal />
            <PostModal editPost={postToEdit} onSubmitPostHandler={onSubmitPostHandler} />
            <div className={styles.container}>
                <div className={styles.categoriesContainer}>
                    <Categories />
                </div>
                <div className={styles.postsContainer}>
                    <div className={styles.buttonContainer}>
                        <Button styleButton={styles.createButton} styleSpan='fa fa-plus' onClick={onClickHandler}>
                            Create Post
                        </Button>
                    </div>
                    <Posts/>
                </div>
            </div>
        </React.Fragment>
    )
}

export default FrontPage;