import Comments from './Comments';
import Comment from './Comment';

export {
    Comments,
    Comment
}