import React from 'react';
import { useDispatch } from 'react-redux';

import { Button } from '../../Components';
import * as actionCreators from '../Posts/actionCreators'
import styles from './styles.module.css';

const Comment = ({info, handleVote}) => {
    const dispatch = useDispatch();
    
    const handleCommentVoteClick = (vote) => {
        console.log('vote',vote)
        dispatch(actionCreators.voteComment(info.id, vote));
    }

    return (
        <div >
             <div className={styles.outerVotesContainer}>
                <div className={styles.votesContainer}>
                    <Button onClick={() => handleCommentVoteClick('upVote')} styleButton={[styles.vote, styles.up].join(' ')} styleSpan='fa fa-arrow-up'/>
                    <div style={{color: "gray", marginRight: "5px"}}>{info.voteScore}</div>
                    <Button onClick={() => handleCommentVoteClick('downVote')} styleButton={[styles.vote, styles.down].join(' ')} styleSpan='fa fa-arrow-down'/>
                </div>
            </div>
            <div className={styles.commentContainer}>
                <div className={styles.commentInfo}>
                    <div className={styles.commentAuthor}>
                        <p >Posted by {info.author}</p>
                    </div>
                </div>
                <div className={styles.commentBody}>
                    <p>{info.body}</p>
                </div>
            </div>
            
        </div>
    );
};

export default Comment;