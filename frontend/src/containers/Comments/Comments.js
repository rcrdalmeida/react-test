import React from 'react';
import {useSelector} from 'react-redux';
import Comment from './Comment';

const Comments = (props) => {
    const [postInfo] = useSelector(state => state.postsReducer.posts.filter(post => post.id === props.postId))
    if (!postInfo.comments) return null;
    
    console.log(postInfo.comments)

    return (
        postInfo.comments.map((comment) => <Comment info={comment} key={comment.id} />)
    );
}

export default Comments;