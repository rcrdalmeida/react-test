import React, { useState } from 'react';
import PropTypes from 'prop-types';

import context from './context';


const Form = ({children, onSubmit, info}) => {
    const initialState = info ? {...info} : {}
    const [formValues, setFormValues] = useState(initialState);

    const updateFormField = (fieldName, value) => {
        const obj = {...formValues, [fieldName]: value}
        setFormValues(obj);
    }

    const onSubmitHandler = (event) => {
        onSubmit(formValues);
        event.preventDefault();
    }

    return (
        <form onSubmit={onSubmitHandler}>
            <context.Provider value={{ updateFormField, values: formValues}}>
                {children}
            </context.Provider>
        </form>
    )
}

Form.propTypes = {
    children: PropTypes.any.isRequired,
    onSubmit: PropTypes.func.isRequired
}

export default Form;