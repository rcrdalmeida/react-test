import React, { useContext, useEffect } from 'react';
import context from './context';

/**
 * HOC to wrap fields of the Form
 * @param {*} WrappedField 
 */
const withForm = WrappedField => props => {
    const contextValue = useContext(context);

    /**
     * Updates state of form
     * @param {*} value 
     */
    const updateFieldValue = value => {
        contextValue.updateFormField(props.fieldName, value);
    }

    useEffect(() => {
        let value = '';
        
        if (props.selected) value = props.selected;
        if (props.value) value = props.value;

        updateFieldValue(value);
    }, []);

    return (
        <WrappedField 
            {...props}
            onChange={updateFieldValue}
            value={contextValue.values[props.fieldName]}
            selected={contextValue.values[props.fieldName]}
        />
    )
}

export default withForm;