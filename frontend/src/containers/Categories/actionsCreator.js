import * as actionTypes from './actionsTypes';
import factory from '../../shared/factory';

const BASE_URL = `${process.env.REACT_APP_API_PROTOCOL}://${process.env.REACT_APP_API_HOST}:${process.env.REACT_APP_API_PORT}`;
const CATEGORIES_ENDPOINT = '/categories';

const getCategories = () =>  factory(actionTypes.GET_CATEGORIES_LOADING, actionTypes.GET_CATEGORIES_ERROR, actionTypes.GET_CATEGORIES_SUCCESS, {
        url: BASE_URL + CATEGORIES_ENDPOINT,
        method: 'GET',
});

const setCategory = category => ({
    type: actionTypes.SET_CATEGORY,
    category
})

export {
    getCategories,
    setCategory
}