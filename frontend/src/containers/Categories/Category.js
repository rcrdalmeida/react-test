import React from 'react';
import { useSelector } from "react-redux";

import styles from './styles.module.css';

const Category = ({info, onClick}) => {
    const currentCategory = useSelector(state => state.categoriesReducer.category);

    let css = currentCategory === info ? [styles.category] : [styles.category,styles.selected];

    return (
        <div onClick={onClick} className={css.join(' ')}>
            <h3>
                {info}
            </h3>
        </div>
    )
}

export default Category;