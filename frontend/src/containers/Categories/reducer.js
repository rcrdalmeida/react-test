import * as actionTypes from './actionsTypes';

const initialState = {
    categories: [{
        name: 'All',
        path: ''
    }],
    category: 'All'
};

const error = (state, action) => ({
    ...state,
    [action.type]: {
        error: !!action.error,
        message: action.error.message
    }
});

const loading = (state, action) => ({
    ...state,
    [action.type]: action.isLoading
});

const getCategories = (state, action) => ({
    ...state,
    categories: [...state.categories, ...action.response.categories]
});

const setCategory = (state, action) => ({
    ...state,
    category: action.category
});

const reducer = (state = initialState, action) => {
    const mapper = {
        [actionTypes.GET_CATEGORIES_ERROR]: error,
        [actionTypes.GET_CATEGORIES_LOADING]: loading,
        [actionTypes.GET_CATEGORIES_SUCCESS]: getCategories,
        [actionTypes.SET_CATEGORY]: setCategory
    }   

    return mapper[action.type] ? mapper[action.type](state, action) : state;
};

export default reducer;