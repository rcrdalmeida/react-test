import React, {useEffect} from 'react';
import {useDispatch, useSelector} from 'react-redux';

import Category from './Category';
import * as actionCreators from './actionsCreator';

const Categories = () => {
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(actionCreators.getCategories());
    }, [dispatch]);

    const categories = useSelector((state) => state.categoriesReducer.categories);

    return (
        <React.Fragment>
           {categories.map(category => (
            <Category 
                key={category.name+category.path} 
                info={category.name}
                onClick={() => {
                    dispatch(actionCreators.setCategory(category.name));
                }}
            />))}
        </React.Fragment>
    );
}

export default Categories;