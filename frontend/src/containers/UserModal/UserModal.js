import React, {useEffect} from 'react';

import {useModal} from '../useModal';

import { Modal, Input, Button } from '../../Components';
import { Form, withForm } from '../Form';
import styles from './styles.module.css';

const Fields = {
    Input: withForm(Input)
}

const UserModal = () => {
    const [user, setUser] = useModal('user');

    useEffect(() => {
        setUser(!sessionStorage.user);
    }, [setUser]);

    const onSubmitHandler = (values) => {
        sessionStorage.setItem('user', values.user);
        setUser(false);
    }

    return (
        <div>
            {user && <Modal closable={false} styleContent={{width: "300px"}}>
                    <Form onSubmit={onSubmitHandler}>
                        <p>What is your username?</p>
                        <Fields.Input 
                            fieldName='user'
                            required
                        />
                        <Button styleButton={styles.button}>Submit</Button>
                    </Form>
                </Modal> 
            }
        </div>
    );
}

export default UserModal;