import * as actionTypes from './actionTypes';

const initialState = {
    posts: [],
    selectedPost: ''
}

const error = (state, action) => ({
    ...state,
    [action.type]: {
        error: !!action.error,
        message: action.error.message
    }
});

const loading = (state, action) => ({
    ...state,
    [action.type]: action.isLoading
});

const getPosts = (state, action) => ({
    ...state,
    posts: action.response
});

const submitPosts = (state, action) => ({
    ...state,
    posts: [...state.posts, action.response]
});

const selectPost = (state, action) => ({
    ...state,
    selectedPost: action.selectedPost
});

const updatePost = (state, action) => ({
    ...state,
    posts: state.posts.map(post => {
        if (post.id !== action.response.id) {
          return post;
        }
        return action.response;
      })
})

const deletePost = (state, action) => ({
    ...state,
    posts: state.posts.map(post => {
        if (post.id !== action.response.id) {
          return post;
        }
        return action.response;
      })
})

const votePost = (state, action) => ({
    ...state,
    posts: state.posts.map(post => {
        if (post.id !== action.response.id) {
          return post;
        }

        return {...post, voteScore: action.response.voteScore};
      })
})

const voteComment = (state, action) => ({
    ...state,
    posts: state.posts.reduce((acc, post) => {
        if (action.response && action.response.parentId === post.id) {
            return [...acc, {...post, comments: post.comments.map(comment => {
                if (comment.id !== action.response.id) {
                  return comment;
                }
                return action.response;
              })}]
        }

        return [...acc, post]
    }, [])
})

const getCommentsPost = (state, action) => ({
    ...state,
    posts: state.posts.reduce((acc, post) => {
        if (action.response.length && action.response[0].parentId === post.id) {
            return [...acc, {...post, comments: action.response}]
        }

        return [...acc, post]
    }, [])
});

const toggleComments = (state, action) => ({
    ...state,
    posts: state.posts.map(post => {

        if (post.id !== action.id) {
          return post;
        }
        console.log({...post, open: !post.open})
        return {...post, open: !post.open};
      })
})

const reducer = (state = initialState, action) => {
    const mapper = {
        [actionTypes.GET_POSTS_ERROR]:error,
        [actionTypes.GET_POSTS_LOADING]: loading,
        [actionTypes.GET_POSTS_SUCCESS]: getPosts,
        [actionTypes.SUBMIT_POST_ERROR]: error,
        [actionTypes.SUBMIT_POST_LOADING]: loading,
        [actionTypes.SUBMIT_POST_SUCCESS]: submitPosts,
        [actionTypes.SET_SELECTED_POST]: selectPost,
        [actionTypes.UPDATE_POST_SUCCESS]: updatePost,
        [actionTypes.UPDATE_POST_LOADING]: loading,
        [actionTypes.UPDATE_POST_ERROR]: error,
        [actionTypes.DELETE_POST_LOADING]: loading,
        [actionTypes.DELETE_POST_ERROR]: error,
        [actionTypes.DELETE_POST_SUCCESS]: deletePost,
        [actionTypes.VOTE_POST_SUCCESS]: votePost,
        [actionTypes.VOTE_POST_LOADING]: loading,
        [actionTypes.VOTE_POST_ERROR]: error,
        [actionTypes.VOTE_COMMENT_SUCCESS]: voteComment,
        [actionTypes.VOTE_COMMENT_LOADING]: loading,
        [actionTypes.VOTE_COMMENT_ERROR]: error,
        [actionTypes.GET_POST_COMMENTS_ERROR]:error,
        [actionTypes.GET_POST_COMMENTS_LOADING]: loading,
        [actionTypes.GET_POST_COMMENTS_SUCCESS]: getCommentsPost,
        [actionTypes.TOGGLE_COMMENTS]: toggleComments
    };

    return mapper[action.type] ? mapper[action.type](state, action) : state;
};

export default reducer;