import React from 'react';
import {useSelector} from 'react-redux';
import Post from './Post';

const Posts = () => {
    const category = useSelector((state) => state.categoriesReducer.category);

    const posts = useSelector((state) => state.postsReducer.posts);

    const filteredPosts = posts.filter(post => (!category || category === 'All') || post.category === category);

    return (
        filteredPosts.map((post) => !post.deleted && <Post info={post} key={post.id}/>)
    );
}

export default React.memo(Posts);