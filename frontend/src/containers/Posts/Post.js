import React, {useEffect} from 'react';
import { useDispatch } from 'react-redux';

import { Button } from '../../Components';
import {useModal} from '../useModal';
import { Comments } from '../Comments';
import styles from "./styles.module.css";
import * as actionCreators from './actionCreators';

const Post = React.memo(({info}) => {
    const [, setPostModal] = useModal('post');
    const dispatch = useDispatch();

    useEffect(() => {
        if (info.commentCount > 0 && !info.comments) dispatch(actionCreators.getPostComments(info.id));
    },[]);
 
    const handleEditClick = () => {
        setPostModal(true);
        dispatch(actionCreators.setSelectedPost(info.id));
    }

    const handleDeleteClick = () => {
        dispatch(actionCreators.deletePost(info.id));
    }

    const handleVoteClick = (vote) => {
        dispatch(actionCreators.votePost(info.id, vote));
    }

    const handleComments = () => {
        dispatch(actionCreators.toggleComments(info.id))
    }

    return (
        <div className={styles.Post}>
            <div className={styles.outerVotesContainer}>
                <div className={styles.votesContainer}>
                    <Button onClick={() => handleVoteClick('upVote')} styleButton={[styles.vote, styles.up].join(' ')} styleSpan='fa fa-arrow-up'/>
                    <div style={{color: "gray", marginRight: "5px"}}>{info.voteScore}</div>
                    <Button onClick={() => handleVoteClick('downVote')} styleButton={[styles.vote, styles.down].join(' ')} styleSpan='fa fa-arrow-down'/>
                </div>
            </div>
            <div className={styles.postContainer}>
                <div className={styles.postInfo}>
                    <div className={styles.postAuthor}>
                        <p >Posted by {info.author} on <u>{info.category}</u></p>
                    </div>
                    <div className={styles.opsContainer}>
                        {info.author === sessionStorage.getItem('user') && <Button onClick={handleEditClick} styleButton={styles.ops} styleSpan='fa fa-pencil-square-o'/>}
                        {info.author === sessionStorage.getItem('user') && <Button onClick={handleDeleteClick} styleButton={styles.ops} styleSpan='fa fa-trash'/>}
                    </div>
                </div>
                <div className={styles.postTitle}>
                    <h3>{info.title}</h3>
                </div>
                <div className={styles.postBody}>
                    <p>{info.body}</p>
                </div>
                <div className={styles.comments}>
                    <Button onClick={handleComments} styleButton={styles.commentButton} styleSpan='fa fa-envelope'>
                        {info.commentCount} comments
                    </Button>
                </div>
                
            </div>
            {info.open && <Comments postId={info.id}/> }
        </div>
    );
});

export default Post;