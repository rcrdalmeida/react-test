const GET_CATEGORY_POSTS_SUCCESS = 'api/GET_CATEGORY_POSTS_SUCCESS';
const GET_CATEGORY_POSTS_LOADING = 'api/GET_CATEGORY_POSTS_LOADING';
const GET_CATEGORY_POSTS_ERROR = 'api/GET_CATEGORY_POSTS_ERROR';

const GET_POSTS_SUCCESS = 'api/GET_POSTS_SUCCESS';
const GET_POSTS_LOADING = 'api/GET_POSTS_LOADING';
const GET_POSTS_ERROR = 'api/GET_POSTS_ERROR';

const SUBMIT_POST_SUCCESS = 'api/SUBMIT_POST_SUCCESS';
const SUBMIT_POST_LOADING = 'api/SUBMIT_POST_LOADING';
const SUBMIT_POST_ERROR = 'api/SUBMIT_POST_ERROR';

const UPDATE_POST_SUCCESS = 'api/UPDATE_POST_SUCCESS';
const UPDATE_POST_LOADING = 'api/UPDATE_POST_LOADING';
const UPDATE_POST_ERROR = 'api/UPDATE_POST_ERROR';

const DELETE_POST_SUCCESS = 'api/DELETE_POST_SUCCESS';
const DELETE_POST_LOADING = 'api/DELETE_POST_LOADING';
const DELETE_POST_ERROR = 'api/DELETE_POST_ERROR';

const VOTE_POST_SUCCESS = 'api/VOTE_POST_SUCCESS';
const VOTE_POST_LOADING = 'api/VOTE_POST_LOADING';
const VOTE_POST_ERROR = 'api/VOTE_POST_ERROR';

const VOTE_COMMENT_SUCCESS = 'api/VOTE_COMMENT_SUCCESS';
const VOTE_COMMENT_LOADING = 'api/VOTE_COMMENT_LOADING';
const VOTE_COMMENT_ERROR = 'api/VOTE_COMMENT_ERROR';

const GET_POST_COMMENTS_SUCCESS = 'api/GET_POST_COMMENTS_SUCCESS';
const GET_POST_COMMENTS_LOADING = 'api/GET_POST_COMMENTS_LOADING';
const GET_POST_COMMENTS_ERROR = 'api/GET_POST_COMMENTS_ERROR';

const SET_SELECTED_POST= 'SET_SELECTED_POST';
const GET_SELECTED_POST= 'GET_SELECTED_POST';

const TOGGLE_COMMENTS='TOGGLE_COMMENTS';

export {
    GET_CATEGORY_POSTS_ERROR,
    GET_CATEGORY_POSTS_LOADING,
    GET_CATEGORY_POSTS_SUCCESS,
    GET_POSTS_ERROR,
    GET_POSTS_LOADING,
    GET_POSTS_SUCCESS,
    SUBMIT_POST_ERROR,
    SUBMIT_POST_LOADING,
    SUBMIT_POST_SUCCESS,
    SET_SELECTED_POST,
    GET_SELECTED_POST,
    UPDATE_POST_ERROR,
    UPDATE_POST_LOADING,
    UPDATE_POST_SUCCESS,
    DELETE_POST_ERROR,
    DELETE_POST_SUCCESS,
    DELETE_POST_LOADING,
    VOTE_POST_ERROR,
    VOTE_POST_SUCCESS,
    VOTE_POST_LOADING,
    GET_POST_COMMENTS_SUCCESS,
    GET_POST_COMMENTS_LOADING,
    GET_POST_COMMENTS_ERROR,
    VOTE_COMMENT_ERROR,
    VOTE_COMMENT_LOADING,
    VOTE_COMMENT_SUCCESS,
    TOGGLE_COMMENTS,
};