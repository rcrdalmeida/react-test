import * as uuid from 'uuid';

import * as actionTypes from './actionTypes';
import factory from '../../shared/factory';

const BASE_URL = `${process.env.REACT_APP_API_PROTOCOL}://${process.env.REACT_APP_API_HOST}:${process.env.REACT_APP_API_PORT}`;

const getPosts = () => factory(actionTypes.GET_POSTS_LOADING, actionTypes.GET_POSTS_ERROR, actionTypes.GET_POSTS_SUCCESS, {
    url: `${BASE_URL}/posts`,
    method: 'GET'
});

const setSelectedPost = selectedPost => ({
    type: actionTypes.SET_SELECTED_POST,
    selectedPost
})

const getPostById = postId => ({
    type: actionTypes.GET_SELECTED_POST,
    id: postId
});

const toggleComments = postId => ({
    type: actionTypes.TOGGLE_COMMENTS,
    id: postId
})

const submitPost = (post) => factory(
    actionTypes.SUBMIT_POST_LOADING,
    actionTypes.SUBMIT_POST_ERROR,
    actionTypes.SUBMIT_POST_SUCCESS,
    {
        url: `${BASE_URL}/posts`,
        method: 'POST',
        data: {
            id: uuid.v4(),
            title: post.title,
            body: post.body,
            author: post.author,
            category: post.category,
            timestamp: Date.now()
        }
    }
);

const updatePost = (id, post) => factory(
    actionTypes.UPDATE_POST_LOADING,
    actionTypes.UPDATE_POST_ERROR,
    actionTypes.UPDATE_POST_SUCCESS,
    {
        url: `${BASE_URL}/posts/${id}`,
        method: 'PUT',
        data: {
            title: post.title,
            body: post.body,
        }
    }
)

const deletePost = (id) => factory(
    actionTypes.DELETE_POST_LOADING,
    actionTypes.DELETE_POST_ERROR,
    actionTypes.DELETE_POST_SUCCESS,
    {
        url: `${BASE_URL}/posts/${id}`,
        method: 'DELETE',
    }
)

const votePost = (id, op) => factory(
    actionTypes.VOTE_POST_LOADING,
    actionTypes.VOTE_POST_ERROR,
    actionTypes.VOTE_POST_SUCCESS,
    {
        url: `${BASE_URL}/posts/${id}`,
        method: 'POST',
        data: {
            option: op
        }
    }
)

const voteComment = (id, op) => factory(
    actionTypes.VOTE_COMMENT_LOADING,
    actionTypes.VOTE_COMMENT_ERROR,
    actionTypes.VOTE_COMMENT_SUCCESS,
    {
        url: `${BASE_URL}/comments/${id}`,
        method: 'POST',
        data: {
            option: op
        }
    }
)


const getPostComments = (id) => factory(
    actionTypes.GET_POST_COMMENTS_LOADING,
    actionTypes.GET_POST_COMMENTS_ERROR,
    actionTypes.GET_POST_COMMENTS_SUCCESS,
    {
        url: `${BASE_URL}/posts/${id}/comments`,
        method: 'GET',
    }
)

export {
    getPosts,
    submitPost,
    setSelectedPost,
    getPostById,
    updatePost,
    deletePost,
    votePost,
    getPostComments,
    voteComment,
    toggleComments
}