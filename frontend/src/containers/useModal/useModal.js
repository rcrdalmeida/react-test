import {useState, useEffect, useContext} from 'react';

import context from './context';

function useModal(value) {
    const contextValue = useContext(context);
    const [inputValue, setInputValue] = useState(false);

    if (!contextValue.subscribers[value]) {
        contextValue.subscribers[value] = [];
    }

    useEffect(() => {
        /**
         * Add subscriber
         */
        contextValue.subscribers[value] = [...contextValue.subscribers[value], setInputValue];

         /**
          * Remove subscriber
          */
        return () => {
            contextValue.subscribers[value] = contextValue.subscribers[value].filter((subs, index) => index !== contextValue.subscribers[value].length - 1);
        }
    }, [contextValue.subscribers, value]);

    const subscribersHandler = (val) => {
        contextValue.subscribers[value].forEach((sub) => sub(val));
    }

    return [inputValue, subscribersHandler]
}

export default useModal;
